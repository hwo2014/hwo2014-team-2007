from bots.noobbot import NoobBot
import socket
import sys


def run_on_track(params, track=None):
    host, port, name, key = params
    print("Connecting with parameters:")
    print("host={0}, port={1}, bot name={2}, key={3}".format(*params))
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, int(port)))
    bot = NoobBot(s, name, key)
    bot.set_socket(s)
    bot.run(track)


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        params = sys.argv[1:5]
        # maps = ["usa", "keimola", "germany"]
        #for map in maps:
        run_on_track(params)
