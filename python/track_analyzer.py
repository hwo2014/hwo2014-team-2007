import math


class TrackAnalyzer(object):
    # Track data
    track = None
    lanes = None

    curves = []
    switches = []

    def load_track(self, pieces, lanes):
        """
        Loads the track, saves the lanes, determines the curves and switches
        """
        self.track = pieces
        self.lanes = lanes

        for index, piece in enumerate(self.track):
            if 'angle' in piece:
                self.curves.append(index)
            if 'switch' in piece:
                self.switches.append(index)

    def get_curves(self):
        """
        Return all curve piece indexes
        """
        return self.curves

    def get_switches(self):
        """
        Return all switch piece indexes
        """
        return self.switches

    def piece_length(self, index, lane):
        """
        Calculates the piece length on given lane
        """
        # TODO Working correctly, see distance calculation?
        if index >= self.track_length():
            index = 0

        if self.is_curve(index):
            if self.turns_right(index):
                radius = self.track[index]["radius"] - self.lanes[lane]["distanceFromCenter"]
                return 2.0 * math.pi * float(radius) * (abs(float(self.track[index]["angle"])) / 360.0)
            else:
                radius = self.track[index]["radius"] + self.lanes[lane]["distanceFromCenter"]
                return 2.0 * math.pi * float(radius) * (abs(float(self.track[index]["angle"])) / 360.0)
        else:
            return self.track[index]["length"]

    def turns_right(self, index):
        piece = self.track[index]
        if piece["angle"] > 0.0:
            return True
        else:
            return False

    def is_curve(self, index):
        """
        Determines if given piece is curved
        """
        return index in self.curves

    def is_switch(self, index):
        """
        Determines if given piece is a piece with a switch
        """
        return index in self.switches

    def track_length(self):
        """
        Track length in pieces
        """
        return len(self.track)

    def next_curve(self, index):
        if len(self.curves) > 0:
            if index >= max(self.curves):
                return min(self.curves)
            else:
                possible_curves = [x for x in self.curves if x > index]
                return min(possible_curves)
        return None

    def next_switch(self, index):
        if len(self.switches) > 0:
            if index >= max(self.switches):
                return min(self.switches)
            else:
                possible_switches = [x for x in self.switches if x > index]
                return min(possible_switches)
        return None

    def distance_to_piece(self, current_piece, piecedistance, target_piece, lane):
        """
        Get the distance to a given piece.
        """

        # TODO Gives negative values. Bug in a piece length calculation or here?

        if target_piece > self.track_length():
            return None

        if target_piece > current_piece:
            pieces = range(current_piece, target_piece)
        else:
            pieces = []
            pieces.extend(range(current_piece, self.track_length()))
            pieces.extend(range(0, target_piece))

        distance = 0.0
        for piece in pieces:
            distance += float(self.piece_length(piece, lane))

        distance -= piecedistance

        return distance
    
    def breaking_force(self, current_speed, throttle = 0.0):
        return (current_speed - throttle * 10) * 2 / 100.0
    
    def break_distance(self, current_speed, target_speed, throttle = 0.0):
        # calculates break distance
        future_speed = current_speed
        distance = future_speed * 2
        while future_speed > target_speed:
            tmp_speed = future_speed
            future_speed -= self.breaking_force(current_speed, throttle)
            distance += (tmp_speed + future_speed) / 2.0
        
        return distance