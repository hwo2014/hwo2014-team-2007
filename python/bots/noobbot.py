from bots.basebot import BaseBot
from track_analyzer import TrackAnalyzer
import math


class NoobBot(BaseBot):
    socket = None

    throttleval = 1.0
    throttlemax = 1.0
    throttlemin = 0.01

    tick = None

    pieces = []
    lanes = []

    current_lane = 0

    previous_piece = 0
    previous_piecedistance = 0.0

    previous_speed = 0.0
    previous_angle = 0.0

    # Todo: Get next curve
    # Todo: Get next switch
    # Todo: Get best lane for speed

    track = None

    def __init__(self, socket, name, key):
        super(NoobBot, self).__init__(socket, name, key)
        self.track = TrackAnalyzer()

    def switch_lane(self, lane):
        self.msg("switchLane", lane)

    def on_game_init(self, data):

        # Initialize track analyzer
        self.pieces = data['race']['track']['pieces']
        self.lanes = data['race']['track']['lanes']

        self.track.load_track(self.pieces, self.lanes)

        print "Track: ", data['race']['track']['name']
        print "Track pieces: ", self.pieces

        print "Curves: ", self.track.get_curves()
        print "Switches: ", self.track.get_switches()

    def calculate_speed(self, piece, piecedistance):
        if self.previous_piece == piece:
            return piecedistance - self.previous_piecedistance
        else:
            return self.previous_speed
            #return self.get_piece_length(self.previous_piece) - self.previous_piecedistance + piecedistance

    def calculate_piece_max_speed(self, piece):
        if piece >= len(self.pieces):
            piece = 0

        if self.track.is_curve(piece):
            return 0.3 + (1 - self.pieces[piece]["angle"] / 180.0) * 2.3 + math.sqrt(self.pieces[piece]["radius"]/5.5)
        else:
            return 10.0

    def on_car_positions(self, data):

        for car in data:
            if car['id']['name'] == self.name:
                angle = car['angle']
                piece = car['piecePosition']['pieceIndex']
                piecedistance = car['piecePosition']['inPieceDistance']
                self.current_lane = car['piecePosition']['lane']['startLaneIndex']

        speed = self.calculate_speed(piece, piecedistance)
        current_piece_max_speed = self.calculate_piece_max_speed(piece)
        next_curve = self.track.next_curve(piece)
        next_curve_max_speed = self.calculate_piece_max_speed(next_curve)
        #next_piece_max_speed = self.calculate_piece_max_speed(piece + 1)
        corner_speed_difference = speed - next_curve_max_speed
        distance_to_next_curve = self.track.distance_to_piece(piece, piecedistance, next_curve, self.current_lane)
        break_distance = self.track.break_distance(speed, next_curve_max_speed)
        angle_diff = max([self.previous_angle, angle]) - min([self.previous_angle, angle])
        
        """ tries to break when approaching curve. greater the speed difference between current speed
            and the maximum speed of the next piece, sooner we start to break. """
        if corner_speed_difference > 0 and distance_to_next_curve <= break_distance:
            self.throttleval = self.throttlemin
        elif abs(angle) < 3 and angle_diff < 3.5:
            # if angle is small then we can do just about anything if our current speed is in the limit
            if speed > current_piece_max_speed:
                self.throttleval = current_piece_max_speed / 10.0 - 0.05 * (abs(angle) - abs(self.previous_angle))
            else:
                self.throttleval = self.throttlemax
        elif abs(angle) < 50:
            # if angle is big enough then try to adjust the angle so that it remains stable
            if angle_diff > 3.5:
                self.throttleval = 0.01
            elif angle_diff > 2.2 or abs(angle) > 45:
                self.throttleval -= 0.1 * angle_diff
            elif angle_diff > 1.2 or abs(angle) > 40:
                self.throttleval = current_piece_max_speed / 10.0
            else:
                self.throttleval += 0.25 * angle_diff
        else:
            # if nothing else does not prevent going nuts then throttle to minimum
            self.throttleval = self.throttlemin

        self.throttleval = min(self.throttleval, self.throttlemax)
        self.throttleval = max(self.throttleval, self.throttlemin)

        # print self.tick, "Piece: ", piece, piecedistance, "Angle: ", angle, "Throttle: ", self.throttleval, "Speed: ", speed, "Next speed: ", next_curve_max_speed
        print self.tick, " Piece:", piece, "{:.2f}".format(piecedistance), " Angle:", "{:.2f}".format(
            angle), " Throttle:", "{:.2f}".format(self.throttleval), " Speed:", "{:.2f}".format(
            speed), " Next speed:", "{:.2f}".format(next_curve_max_speed), " Current length:", "{:.2f}".format(
            self.track.piece_length(piece, self.current_lane)), "N curve", self.track.next_curve(
            piece), "N switch", self.track.next_switch(piece), "Dist to Curv:", "{:.2f}".format(distance_to_next_curve
            ), "Break dist:", "{:.2f}".format(break_distance)

        if piece != self.previous_piece:
            # Check lane switching
            if self.track.is_switch(piece+1):
                nc = self.track.next_curve(piece)
                if self.track.turns_right(nc):
                    print "Switch right"
                    self.switch_lane("Right")
                else:
                    print "Switch left"
                    self.switch_lane("Left")

        self.throttle(self.throttleval)

        self.previous_piece = piece
        self.previous_piecedistance = piecedistance
        self.previous_speed = speed
        self.previous_angle = angle
