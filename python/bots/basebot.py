import json


class BaseBot(object):
    """
    HWO Basic bot and communication
    """
    socket = None

    turbo_available = False
    turbo_factor = 0
    turbo_ticks = 0
    turbo_milliseconds = 0

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

    def set_socket(self, socket):
        self.socket = socket

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def create_race(self, track):
        return self.msg("createRace", {
            "botId": {
                "name": self.name,
                "key": self.key
            },
            "trackName": track,
            "carcount": 1

        })

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self, track=None):
        if track is not None:
            self.create_race(track)
        else:
            self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_game_init(self, data):
        self.ping()

    def on_car_positions(self, data):
        self.throttle(0.5)

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def on_turbo_available(self, data):
        self.turbo_available = True
        self.turbo_factor = data["turboFactor"]
        self.turbo_milliseconds = data["turboDurationMilliseconds"]
        self.turbo_ticks = data["turboDurationTicks"]

        print "TURBO AVAILABLE! Factor:", self.turbo_factor, "Duration (ticks)", self.turbo_ticks, "(milliseconds)", self.turbo_milliseconds

        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.on_game_init,
            'turboAvailable': self.on_turbo_available
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']

            if msg_type == "carPositions":
                try:
                    self.tick = msg['gameTick']
                except:
                    print "No gametick!"
                    self.tick = 0

            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()